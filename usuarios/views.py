from django.shortcuts import render
from django.http import HttpResponse, JsonResponse

from usuarios.models import Usuarios

# Create your views here.
def holaMundo(request):
    
    usuarios = Usuarios.objects.all()
    
    usuarios_ordenados = []
    
    for usuario in usuarios:
        
        row = {}
        
        row["rutUsuario"] = usuario.rutUsuario
        row["nombre"] = usuario.nombre
        
        usuarios_ordenados.append(row)
    
    # return HttpResponse("Hola")
    return JsonResponse(usuarios_ordenados, safe=False)
    