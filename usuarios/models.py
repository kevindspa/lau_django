from django.db import models

# Create your models here.
class Usuarios(models.Model):
    rutUsuario = models.CharField(primary_key=True, max_length=255)
    nombre = models.CharField(max_length=255)